# Setup of a raspberry pi

These steps have only been tested on Linux.

## prerequisites

* rpi-imager

## Base-Install

1. Start rpi-imager.
2. Select "Ubuntu Server 22.04 LTS 64-bit"
3. Configure Network and setup user "pi" with password "pi" (will be changed later)
4. Write image
5. Start in Raspberry, while connected to network. (Wait 5 Minutes, than reboot!)

## known Issues

### No connection after first boot

Solution: Wait 5 Minutes and reboot (turn power off, turn on again).

TODO: Continue here.
