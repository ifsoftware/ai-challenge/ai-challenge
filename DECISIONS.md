# Entscheidungen

## JetBot vs. DonkeyCar vs. OpenBot

JetBot: Jetson Nano auf kleiner Plattform
DonkeyCar: "normales" ferngesteuertes Auto mit Jetson Nano
OpenBot: Handy statt Jetson als Steuerung

Entscheidung: Wir wählen **x** weil **y**.

### Entscheidungen

* Wir wählen OpenBot **nicht**, da zu wenig Anspruchsvoll.
* Wir wählen Jetson Nano, da eine GPU praktisch ist
* Wir wählen unser eigenes Fahrzeug, da Repariebrarkeit und Bundle deutlich günstiger ist
* Wir kürzen die Veranstaltung auf 8 Wochen, damit es vor Weihnachten fertig sein kann

## generelle Entscheidungen

1. Sponsoren: Claudia, Markus

* Sponsoren: Ja, wenn möglich. Könnte zB sogar sehr direkt sein: Sponsoren Logo auf JetBot/DonkeyCar?
* Rollups an Sponsoren-Tisch, Preisverlehung und Begrüssung
* Begrüssung/Abschluss auf Folien
* Rennstrecke

