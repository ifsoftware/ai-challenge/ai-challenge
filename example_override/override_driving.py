import os
import time
from pathlib import Path
import onnxruntime as ort
from torchvision import transforms

convert_tensor = transforms.ToTensor()

path = Path(os.path.dirname(os.path.realpath(__file__)))

model = path / 'model.onnx'
ORT_SESSION = ort.InferenceSession(str(model))


def predict_next_input(image):
    global ORT_SESSION
    tensor_img = convert_tensor(image).unsqueeze(0).numpy()
    result = ORT_SESSION.run(None, {'image': tensor_img})
    steering_data = result[0][0]
    return float(steering_data)


def predict_throttle_speed(image):
    steering = predict_next_input(image)
    throttle = 0.05
    return throttle, steering
