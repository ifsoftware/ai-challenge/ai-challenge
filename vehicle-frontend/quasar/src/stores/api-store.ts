import { defineStore } from 'pinia';
import {
  Configuration,
  VehicleApiFactory,
  ISystemStats,
  IStatusInfo,
  IRecordingFolder,
  IContainerInfo,
  IModelFolder,
} from 'src/lib/generated-axios-client';
import { api } from 'boot/axios';

const conf = new Configuration();
const client = VehicleApiFactory(conf, '', api);

type RootState = {
  vehicleStats: ISystemStats | null,
  serviceStatus: IStatusInfo | null,
  apiError: Record<string, unknown>,
  recordingFolders: IRecordingFolder[],
  statsFetchingLoopEnabled: boolean,
  containerStatuses: IContainerInfo[] | [],
  containerLog: string,
  containerLogFetchedOn: number,
  modelFolders: IModelFolder[],
  errors: Record<string, unknown>,
};

export const useApiStore = defineStore('apiStore', {
  state: () => ({
    vehicleStats: null,
    apiError: {},
    serviceStatus: null,
    recordingFolders: [],
    statsFetchingLoopEnabled: false,
    containerStatuses: [],
    containerLog: '',
    containerLogFetchedOn: 0,
    modelFolders: [],
    errors: {},
  } as RootState),
  actions: {
    async updateSystemStats() {
      try {
        this.vehicleStats = await (
          await client.vehicleSystemStatsResponseApiSystemStatsGet()
        ).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async updateServiceStatus() {
      try {
        this.serviceStatus = await (
          await client.vehicleLocalStatsResponseApiStatsGet()
        ).data;
        this.errors = this.serviceStatus?.errors || {};
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async switchRecordingState() {
      try {
        this.serviceStatus = await (
          await client.flipRecordingSwitchApiSwitchRecordingPost()
        ).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async switchDrivingMode() {
      try {
        this.serviceStatus = await (
          await client.switchDrivingModeEndpointApiSwitchDrivingModePost()
        ).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async updateRecordFolderList() {
      try {
        this.recordingFolders = await (
          await client.listRecordingFoldersApiRecordingsListGet()
        ).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async deleteFolder(folder: IRecordingFolder) {
      try {
        await client.deleteRecordsFolderApiRecordingsRemoveDelete(folder);
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async startFetchStatsLoop(iAmTheOne: boolean | undefined) {
      if (!this.statsFetchingLoopEnabled || iAmTheOne) {
        // something akin a singleton, refactor a later stage
        this.statsFetchingLoopEnabled = true;

        // Reconnect in 1.5s
        const RUN_EVERY_MS = 1500;

        // eslint-disable-next-line no-promise-executor-return
        await new Promise((resolve) => setTimeout(resolve, RUN_EVERY_MS));
        await this.updateServiceStatus();
        await this.updateSystemStats();
        await this.startFetchStatsLoop(true);
      }
    },
    async getContainerStatuses() {
      try {
        this.containerStatuses = await (await client.dockerContainersApiContainersGet()).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async deleteModel(folder: IModelFolder) {
      try {
        await client.deleteModelFolderApiModelsRemoveDelete(folder);
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async updateModelFolderList() {
      try {
        this.modelFolders = await (
          await client.modelsListApiModelsGet()
        ).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async setActiveModel(model: IModelFolder) {
      try {
        this.serviceStatus = await (
          await client.activateModelApiModelsActivatePost(model)
        ).data;
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.vehicleStats = error;
      }
    },
    async uploadNewModel(file: File) {
      try {
        await client.uploadModelApiUploadModelPost(file);
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        // TODO: display the error
        this.apiError.models = error;
        throw error;
      }
    },
  },
});
