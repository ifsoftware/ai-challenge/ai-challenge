import { Notify } from 'quasar';
import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance } from 'axios';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
  }
}

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';

// eslint-disable-next-line
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
axios.defaults.baseURL = '';

// eslint-disable-next-line
const handleError = (error: any) => {
  if (error?.response?.data?.detail) {
    Notify.create({
      message: error?.response?.data?.detail,
      multiLine: true,
      type: 'negative',
    });
  } else {
    Notify.create({
      message: error.message,
      multiLine: true,
      type: 'negative',
    });
  }
};

const api = axios.create();

// api.interceptors.request.use((config) => config, (error) => {
//   handleError(error);
//   return Promise.reject(error);
// });

// Add a response interceptor
api.interceptors.response.use((response) => response, (error) => {
  handleError(error);
  return Promise.reject(error);
});

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios;
  app.config.globalProperties.$api = api;
});

export { api };
