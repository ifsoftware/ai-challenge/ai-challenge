#!/bin/bash

set -e

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

echo 'ensure the api and proxy is running'

wget http://localhost/api/openapi.json --no-check-certificate --header='accept: application/json' -O ${SCRIPTPATH}/schema.json

# docker run -v"${PWD}:/local" --rm -it openapitools/openapi-generator-cli generate -i /local/schema.json -g typescript-axios -o /local/generated-axios
docker run --user "1000:1000" -v "${SCRIPTPATH}:/local" --rm -it openapitools/openapi-generator-cli \
    generate \
    --skip-validate-spec \
    --enable-post-process-file \
    --model-name-prefix I \
    -i /local/schema.json \
    -g typescript-axios \
    -o /local/generated-axios-client
# requires '--skip-validate-spec' because of endpoints /api/core/users/{user_id} and /api/core/i18n/languages/current.
# their 'operationId' is not unique / got repeated.

# to get options:
# docker run --user "1000:1000" -v "${SCRIPTPATH}:/local" --rm -it openapitools/openapi-generator-cli \
# java -jar /opt/openapi-generator/modules/openapi-generator-cli/target/openapi-generator-cli.jar config-help -g typescript-axios


rm -rf ${SCRIPTPATH}/generated-axios-client/.openapi-generator
rm -rf ${SCRIPTPATH}/generated-axios-client/.gitignore
rm -rf ${SCRIPTPATH}/generated-axios-client/.npmignore
rm -rf ${SCRIPTPATH}/generated-axios-client/.openapi-generator-ignore
rm -rf ${SCRIPTPATH}/generated-axios-client/git_push.sh

rm -rf ${SCRIPTPATH}/../quasar/src/lib/generated-axios-client
mv ${SCRIPTPATH}/generated-axios-client ${SCRIPTPATH}/../quasar/src/lib/

rm -f ${SCRIPTPATH}/schema.json
