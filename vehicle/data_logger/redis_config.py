import json
from datetime import datetime
from walrus import Database
from collections import namedtuple


STREAM_NAME = 'steering-throttle-data-stream'

# FIXME: connection definition via ENV-Variables
redis_db = Database(host='redis', port=6379, db=0)

ts = redis_db.time_series(STREAM_NAME, ['remote_c',])
ts.create()
ts.remote_c.set_id('$')

status_info = redis_db.Hash('status_info')

if not status_info.get('is_recording'):
    status_info['is_recording'] = 0

if not status_info.get('training_mode'):
    status_info['training_mode'] = 1

if not status_info.get('is_recording_started'):
    status_info['is_recording_started'] = 0

if not status_info.get('errors'):
    status_info['errors'] = json.dumps({})

if not status_info.get('current_selected_model'):
    status_info['current_selected_model'] = ''


def _get_errs():
    errs = json.loads(status_info.get('errors', '{}'))
    return errs


def set_error(key, error):
    errs = _get_errs()
    errs[key] =  error
    status_info['errors'] = json.dumps(errs)


def reset_error(key):
    errs = _get_errs()
    if key in errs:
        del errs[key]
    status_info['errors'] = json.dumps(errs)


def get_current_selected_model():
    return status_info.get('current_selected_model', b'').decode('utf-8')


def set_current_selected_model(model_folder_path):
    status_info['current_selected_model'] = model_folder_path


def bool_to_int(bool_value: bool):
    bools = [False, True]
    try:
        return bools.index(bool_value)
    except ValueError:
        pass
    return 0


def redis_content_to_bool(val):
    try:
        return bool(int(val))
    except (ValueError, TypeError):
        pass
    return None


def set_is_recording(is_recording: bool):
    status_info.update(is_recording=bool_to_int(is_recording))


def set_initial_recording_switch_state(initial_recording_switch_state: float):
    status_info.update(initial_recording_switch_state=initial_recording_switch_state)


def is_recording():
    return redis_content_to_bool(status_info.get('is_recording', 0))


def is_recording_started():
    return redis_content_to_bool(status_info.get('is_recording_started', 0))


def start_stop_recording_switch():
    status_info.update({'is_recording': bool_to_int(not redis_content_to_bool(status_info.get('is_recording', 0)))})


def set_is_recording_started(is_recording_started: bool):
    status_info.update(is_recording_started=bool_to_int(is_recording_started))


def get_initial_recording_switch_state():
    state = status_info.get('initial_recording_switch_state')
    if not state:
        return None
    return float(state)


def get_status_info():
    ei = status_info
    return {
        "self_driving": not redis_content_to_bool(ei.get('training_mode')),
        "training_mode": redis_content_to_bool(ei.get('training_mode')),
        "model": ei.get('model', None),
        "initial_recording_switch_state": get_initial_recording_switch_state(),
        "is_recording": is_recording(),
        "is_recording_started": redis_content_to_bool(ei.get('is_recording_started', 0)),
        "errors": json.loads(ei.get('errors', '{}')),
        "current_selected_model": ei.get('current_selected_model', ''),
    }


def set_training_mode(training_mode: bool):
    status_info['training_mode'] = bool_to_int(training_mode)


def switch_driving_mode():
    set_training_mode(not redis_content_to_bool(status_info.get('training_mode', True)))


def update_self_driving(self_driving: str):
    if self_driving is None and status_info.get('self_driving'):
        del status_info['self_driving']
    else:
        status_info.update(**{
            "self_driving": self_driving,
        })


def update_model(model: str):
    if model is None and status_info.get('model'):
        del status_info['model']
    else:
        status_info.update(**{
            "model": model,
        })


def remote_control_stream():
    ts.remote_c.trim(count=1000)
    return ts.remote_c


def add_receiver_info(info: dict, id: datetime):
    remote_control_stream().add(
        info, id=id,
    )


def get_messages_since(timestamp):
    # maximum wait time until result should be available
    return ts.remote_c[timestamp::]


def get_latest_message():
    # maximum wait time until result should be available
    return ts.read(block=200)[-1]


def get_latest():
    return get_latest_message().data


OperationStatus = namedtuple('OperationStatus', ['is_training_mode', 'is_recording', 'is_recording_started'])


def get_vehicle_operation_params():
    info = get_status_info()
    return OperationStatus(
        is_training_mode=info.get('training_mode', True),
        is_recording=is_recording(),
        is_recording_started=info.get('is_recording_started', False),
    )
