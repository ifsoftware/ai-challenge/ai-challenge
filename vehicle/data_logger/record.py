import statistics
from typing import List
import cv2
import json
import os
import subprocess

from datetime import datetime
from pathlib import Path
from video_streaming.client import get_single_image
from data_logger.redis_config import get_latest_message, get_messages_since


BASE_PATH = Path(os.environ.get('UPLOADS_BASE_PATH', '/home/pi/uploads'))


def get_median(data: List):
    try:
        return statistics.median(data)
    except statistics.StatisticsError as e:
        print(f"error getting median, resulted in error: {e}")
        return 0


class Recorder:
    def __init__(self):
        self.recording_destination_base_name = BASE_PATH
        self.recording_destination_base_name.mkdir(parents=True, exist_ok=True)
        self._recording_start_time = None
        self._last_message_timestamp = None

    def assign_timestamp(self):
        self._last_message_timestamp = get_latest_message().timestamp

    def _initialize_for_next_recording(self):
        self.assign_timestamp()
        self.index = 0
        now = datetime.now()
        self._recording_start_time = now
        self.recording_destination_base_name = BASE_PATH / str(now.strftime("%d-%m-%Y_%H-%M-%S"))
        self.recording_destination_base_name.mkdir(parents=True, exist_ok=True)

    def get_steering_throttle_values(self):
        # TODO: use redis to store and retrieve inputs
        data = get_messages_since(self._last_message_timestamp)
        self.assign_timestamp()

        if len(data):
            throttle_list = [float(m.data['throttle_data']) for m in data]
            steering_list = [float(m.data['steering_data']) for m in data]
        else:
            throttle_list = [0.0]
            steering_list = [0.0]

        # we need to get this as late as possible b/c of the image lag
        steering = get_median(steering_list)
        throttle = get_median(throttle_list)
        return steering, throttle

    def log_data(self):
        steering, throttle = self.get_steering_throttle_values()
        image = get_single_image()

        if image is not None:
            now = datetime.now()
            iso_date = now.isoformat()
            img_destination = self.recording_destination_base_name / f'{iso_date}.png'
            log_destination = self.recording_destination_base_name / f'{iso_date}.json'
            recording_duration = now - self._recording_start_time

            cv2.imwrite(str(img_destination), image)

            with open(log_destination, 'w', encoding='utf-8') as f:
                data = {
                    "timestamp": str(now),
                    "recording_duration": str(recording_duration),
                    "steering": steering,
                    "throttle": throttle,
                }
                f.write(json.dumps(data))
        else:
            print('failed to aquire image, not writing log')

    def start_recording(self):
        # start video recording wasn't that stable, switched to still images
        self._initialize_for_next_recording()
        print("Started recording")

    def _set_access_rights(self):
        subprocess.run(f'chown -R 1000:1000 {BASE_PATH}', shell=True)

    def stop_recording(self):
        try:
            self._set_access_rights()
        except:
            pass
        print("Stopped recording")

