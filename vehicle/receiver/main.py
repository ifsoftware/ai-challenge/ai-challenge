import time
from datetime import datetime, timedelta

from constants import Config
from controller import RCReceiver

from data_logger.redis_config import add_receiver_info


def wait_for_pigpiod_start():
    print(f'waiting for pigpiod to be up, 10 seconds')
    time.sleep(5)


wait_for_pigpiod_start()

receiver = RCReceiver(Config)


def process_remote_controller_input_data(initial_recording_switch_state):
    now = datetime.now()
    iso = now.isoformat()
    timestamp = int(now.timestamp() * 1000)
    result = receiver.run()
    steering_data, throttle_data, gear_data = result[0], result[1], result[2]
    
    add_receiver_info({
        'timestamp': iso,
        'timestamp_millis': timestamp,
        'steering_data': steering_data,
        'throttle_data': throttle_data,
        'gear_data': gear_data,
    }, id=now)
    return now


def controller_data_collection_loop():
    log_result_every_x_seconds = 10
    now = datetime.now()
    counter = 0

    # original state of recording button (input 3), which is also the gear button
    # wait for receiver to be ready (time wasted to find this bug: 2h)
    time.sleep(0.2)
    result = receiver.run()
    gear_data = result[2]
    initial_recording_switch_state = gear_data

    while True:
        last_processed_on = process_remote_controller_input_data(initial_recording_switch_state)
        counter += 1

        if now + timedelta(seconds=log_result_every_x_seconds) <= last_processed_on:
            now = datetime.now()
            print(f'received and logged approx. {counter} receiver inputs in the last {log_result_every_x_seconds}s (~{int(counter/log_result_every_x_seconds)}/s)')
            counter = 0

        # we need to sleep, otherwise the stream input is submillisecond and
        # results in clashes  with the ids
        # sleep one millisecond
        time.sleep(Config.GENERAL_SLEEP_BETWEEN_UPDATES_SECONDS)


if __name__ == '__main__':
    print(f'starting remote control loop')
    controller_data_collection_loop()
    # FIXME: this should be best practice to shutdown properly
    # after a loop break. Needs to be fixed next year
    # receiver.shutdown()
