import time

from board import SCL, SDA
from adafruit_pca9685 import PCA9685
from adafruit_motor import servo
from constants import Config

import busio


class Vehicle:
    def __init__(self):

        i2c = busio.I2C(SCL, SDA)
        # wait for startup to complete
        time.sleep(2)
        self.pca = PCA9685(i2c)

        # trial and error proved to be a reasnoable guess
        self.pca.frequency = 60
        steering = self.pca.channels[0]
        motor = self.pca.channels[1]
        gear = self.pca.channels[3]
    
        # trial and error proved to be a reasnoable guess
        self.motor = servo.ContinuousServo(motor, min_pulse=1024, max_pulse=2048)
        self.servo = servo.Servo(steering)
        # trial and error proved to be a reasnoable guess
        self.gear = servo.Servo(gear, actuation_range=90, min_pulse=1250, max_pulse=1750)

        # TODO add code for gear change

    def __del__(self):
        self.pca.deinit()

    # This gets a value between -1 and 1, converts it to a value between 0 and 180 (degrees)
    def set_steering(self, steering):
        # TODO figure out how to get values between 1 and -1 instead of ±1.2
        if steering < -1:
            steering = -1
        elif steering > 1:
            steering = 1

        # Steering + 1: always get a positive number | * 45: get a number between 0 and 90 | + 45: move value to the
        # middle of range 0 to 180 (90 is centre) | + config.STEERING_OFFSET: offset, as not all servos are nicely calibrated
        adjusted_steering = (steering + 1) * 45 + 45 + Config.STEERING_OFFSET
        self.servo.angle = adjusted_steering


    # This function expects a value between -1 and 1 and simply passes it to the motor
    def set_throttle(self, throttle):
        # set throttle to 0 (initially, the rc receiver might send a value outside of these bounds. This gets set to 0
        # so that it doesn't suddenly start rolling and so that the script doesn't crash
        if throttle > 1 or throttle < -1:
            throttle = 0

        self.motor.throttle = throttle * Config.THROTTLE_FACTOR

    def set_gear(self, gear):
        if gear > 1:
            gear = 1
        elif gear < 0:
            gear = 0

        # reverse gear (so 1 is fast and 0 is slow)
        gear = 1 if gear == 0 else 0

        self.gear.fraction = gear
