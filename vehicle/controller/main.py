import time

from pathlib import Path
from controller.servo_and_steering import Vehicle

# AttributeError: Custom classes or functions exported with your `Learner` not available in namespace.\Re-declare/import before loading
# This is why we need to imports here. Don't remove!
from self_drive.model_driver import predict_next_step
from data_logger.redis_config import get_latest, get_vehicle_operation_params
from constants import Config

vehicle = Vehicle()


def get_remote_controller_input_data():
    data = get_latest()
    steering_data, throttle_data, gear_data = float(data['steering_data']), float(data['throttle_data']), float(data['gear_data'])
    return steering_data, throttle_data, gear_data


def steer_and_throttle_loop(op_status):
    # always set low gear
    vehicle.set_gear(0)

    while op_status.is_training_mode:
        steering_data, throttle_data, gear_data = get_remote_controller_input_data()

        vehicle.set_steering(steering_data)
        vehicle.set_throttle(throttle_data)
        # vehicle.set_gear(gear_data)

        time.sleep(Config.GENERAL_SLEEP_BETWEEN_UPDATES_SECONDS)
        op_status = get_vehicle_operation_params()


def self_driving_loop(op_status):
    from data_logger.redis_config import set_error, reset_error, set_training_mode
    
    while not op_status.is_training_mode:
        op_status = get_vehicle_operation_params()

        try:
            throttle_data, steering_data = predict_next_step()
            reset_error('self_driving')
        except Exception as err:
            print(err)
            vehicle.set_steering(0)
            vehicle.set_throttle(0)
            set_error('self_driving', str(err))
            print(f'error occurred, stopping vehicle')
            set_training_mode(True)
            break

        vehicle.set_steering(steering_data)
        vehicle.set_throttle(throttle_data)

        print('steering_data, throttle_data')
        print(steering_data, throttle_data)

        time.sleep(Config.GENERAL_SLEEP_BETWEEN_UPDATES_SECONDS)
        op_status = get_vehicle_operation_params()


def run_loop():
    print(f'starting vehicle loop')
    
    while True:
        op_status = get_vehicle_operation_params()
        if not op_status.is_training_mode:
            print(f'enabling self driving')
            self_driving_loop(op_status)
        else:
            print(f'enabling remote control')
            steer_and_throttle_loop(op_status)


if __name__ == '__main__':
    run_loop()
