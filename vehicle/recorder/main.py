import time
from datetime import datetime, timedelta

from constants import Config
from data_logger.record import Recorder
from data_logger.redis_config import get_vehicle_operation_params, set_is_recording_started, set_is_recording

recorder = Recorder()
log_result_every_x_seconds = 10


def start_recording(op_status):
    # start recording
    if not op_status.is_recording_started:
        set_is_recording(True)
        print('starting recording')
        recorder.start_recording()
        set_is_recording_started(True)


def stop_recording(op_status):
    # stop recording
    if op_status.is_recording_started:
        set_is_recording_started(False)
        print('stopping recording')
        recorder.stop_recording()
        set_is_recording(False)


def recording_start_stop(op_status):
    now = datetime.now()
    if op_status.is_recording:
        start_recording(op_status)
        recorder.log_data()
    else:
        stop_recording(op_status)
    return now


def run_loop(*, start_time):
    record_next_run = start_time
    op_status = get_vehicle_operation_params()

    while True:
        counter = 0
        last_processed_on = datetime.now()
        last_log_print_on = datetime.now()
        
        if op_status.is_training_mode:
            now = datetime.now()
            if now >= record_next_run:
                record_next_run = now + timedelta(milliseconds=Config.RECORD_EVERY_X_SECONDS * 1000.0)
                last_processed_on = recording_start_stop(op_status)

            if op_status.is_recording:
                counter += 1
                if last_log_print_on + timedelta(seconds=log_result_every_x_seconds) <= last_processed_on:
                    last_log_print_on = datetime.now()
                    print(f'recorded approx. {counter} images and logfiles in the last {log_result_every_x_seconds}s (~{int(counter/log_result_every_x_seconds)}/s)')
                    counter = 0
            else:
                counter = 0

        else:
            if op_status.is_recording_started:
                print('Not in manual mode, recording disabled')
                stop_recording(op_status)

        time.sleep(Config.GENERAL_SLEEP_BETWEEN_UPDATES_SECONDS)
        op_status = get_vehicle_operation_params()


if __name__ == '__main__':
    run_loop(start_time=datetime.now())
