import sys

from pathlib import Path
from torchvision import transforms
import onnxruntime as ort

from video_streaming.client import get_single_image

convert_tensor = transforms.ToTensor()


def create_ort_session(model_path):
    ort_sess = ort.InferenceSession(model_path)
    return ort_sess


LAST_ACTIVE_MODEL = ''
ORT_SESSION = None
PREDICTION_FUNCTION = None


def predict_next_input(image):
    name = Path(LAST_ACTIVE_MODEL).stem
    model = Path(LAST_ACTIVE_MODEL) / f'{name}.onnx'
    global ORT_SESSION
    if ORT_SESSION is None:
        print(f'loading learner from {model}')
        ORT_SESSION = create_ort_session(f'{model}')
    tensor_img = convert_tensor(image).unsqueeze(0).numpy()
    result = ORT_SESSION.run(None, {'image': tensor_img})
    throttle_data, steering_data = result[0][0]
    return float(throttle_data), float(steering_data)


def predict_next_image(image):
    return predict_next_input(image)


def unload_prediction_function():
    global LAST_ACTIVE_MODEL, ORT_SESSION, PREDICTION_FUNCTION
    ORT_SESSION = None
    PREDICTION_FUNCTION = None
    try:
        sys.path.remove(str(LAST_ACTIVE_MODEL))
    except:
        pass
    try:
        del sys.modules['override_driving']
    except:
        pass
    try:
        del override_driving
    except:
        pass


def get_predictor_function():
    global LAST_ACTIVE_MODEL, ORT_SESSION, PREDICTION_FUNCTION
    from data_logger.redis_config import get_current_selected_model, set_error, reset_error

    if not get_current_selected_model():
        set_error('self_driving_path', f'No model selected')
    else:
        reset_error('self_driving_path')

    selected_model_folder = Path(get_current_selected_model())

    # switch model, unset the poor mans "cache"
    if str(selected_model_folder) != str(LAST_ACTIVE_MODEL):
        unload_prediction_function()

    if PREDICTION_FUNCTION is not None:
        return PREDICTION_FUNCTION
    
    if selected_model_folder:
        if not selected_model_folder.exists():
            set_error('self_driving_path', f'Path to model {selected_model_folder} not found')
            raise ValueError(f'Path to model {selected_model_folder} not found')
        
        reset_error('self_driving_path')
        # import function from custom function, if it exists
        try:
            LAST_ACTIVE_MODEL = selected_model_folder
            sys.path.insert(1, str(selected_model_folder))
            import override_driving
            PREDICTION_FUNCTION = override_driving.predict_throttle_speed
        except ImportError:
            unload_prediction_function()
            PREDICTION_FUNCTION = predict_next_image
        return PREDICTION_FUNCTION


def predict_next_step():
    image = get_single_image()
    predictor = get_predictor_function()
    return predictor(image)
