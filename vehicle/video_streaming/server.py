import os
import os
import uvicorn

from vidgear.gears.asyncio import WebGear


SERVER_IP = str(os.environ.get('VIDEO_STREAMING_HOSTNAME', 'localhost'))
SERVER_PORT = int(os.environ.get('VIDEO_STREAMING_PORT', 8123))


# various performance tweaks
options = {
    # "frame_size_reduction": 40,
    "jpeg_compression_quality": 60,
    "jpeg_compression_fastdct": True,
    "jpeg_compression_fastupsample": True,
    "framerate": 30,
    "custom_data_location": "/app/vehicle/static/",
    "CAP_PROP_FRAME_WIDTH": 320,
    "CAP_PROP_FRAME_HEIGHT": 240,
    "CAP_PROP_FPS": 30,
}


def run_video_streaming_server():
    # initialize WebGear app
    web = WebGear(source=0, logging=True, **options)

    # run this app on Uvicorn server at address http://localhost:SERVER_PORT/
    uvicorn.run(web(), host="0.0.0.0", port=SERVER_PORT, log_level='error')

    # close app safely
    web.shutdown()
