from vidgear.gears import CamGear
from contextlib import contextmanager

import os

SERVER_IP = str(os.environ.get('VIDEO_STREAMING_HOSTNAME', '127.0.0.1'))
SERVER_PORT = int(os.environ.get('VIDEO_STREAMING_PORT', 8123))


@contextmanager
def get_stream():
    options = {
        "CAP_PROP_FRAME_WIDTH": 320,
        "CAP_PROP_FRAME_HEIGHT": 240,
        "CAP_PROP_FPS": 30
    }
    stream = CamGear(source=f'http://{SERVER_IP}:{SERVER_PORT}/video', **options)
    yield stream
    stream.stop()


def get_single_image():
    with get_stream() as stream:
        frame = None
        while frame is None:
            # read frames from stream
            frame = stream.read()
        return frame
