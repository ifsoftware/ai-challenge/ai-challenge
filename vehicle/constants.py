from os import environ
from pathlib import Path


def get_value(env_var_name: str, default_value):
    return environ.get(env_var_name, default_value)


class Config:
    # FOR PGPIO
    PI_GPIO_HOST = str(get_value('PI_GPIO_HOST', ''))
    PI_GPIO_PORT = int(get_value('PI_GPIO_PORT', 8888))

    # For RCReceiver
    STEERING_RC_GPIO = int(get_value('STEERING_RC_GPIO', 26))
    THROTTLE_RC_GPIO = int(get_value('THROTTLE_RC_GPIO', 19))
    GEAR_RC_GPIO = int(get_value('GEAR_RC_GPIO', 13))
    PIGPIO_STEERING_MID = int(get_value('PIGPIO_STEERING_MID', 1500))
    PIGPIO_MAX_FORWARD = int(get_value('PIGPIO_MAX_FORWARD', 2000))
    PIGPIO_STOPPED_PWM = int(get_value('PIGPIO_STOPPED_PWM', 1500))
    PIGPIO_MAX_REVERSE = int(get_value('PIGPIO_MAX_REVERSE', 1000))
    AUTO_RECORD_ON_THROTTLE = False
    PIGPIO_INVERT = bool(get_value('PIGPIO_INVERT', False))
    PIGPIO_JITTER = float(get_value('PIGPIO_JITTER', 0.025))

    # for Vehicle
    STEERING_OFFSET = int(get_value('STEERING_OFFSET', 10))
    # write out the percentage in decimal. E.g. for a max throttle of 50%, set the value to 0.5.
    THROTTLE_FACTOR = float(get_value('THROTTLE_FACTOR', 1.0))

    # for Software
    UPLOADS_BASE_PATH = Path(get_value('UPLOADS_BASE_PATH', '/home/pi/uploads'))
    SELF_DRIVING_MODEL_PATH = get_value('SELF_DRIVING_MODEL_PATH', None)
    MODEL_FOLDER_PATH = UPLOADS_BASE_PATH / 'models'

    RECORD_EVERY_X_SECONDS = float(get_value('RECORD_EVERY_X_SECONDS', 1/10))
    RECORDING_INFO_JSON = Path('/tmp/recording_info.json') # write only!
    RUNNING_MODE_STATUS_JSON = Path('/tmp/vehicle_info.json')  # for all the main_*: read only
    UPDATE_RUNNING_MODE_INFO_X_SECONDS = float(get_value('UPDATE_RUNNING_MODE_INFO_X_SECONDS', 1/3))

    # we had 1/1000, which make the raspi run very hot, 1/100 seems about right
    GENERAL_SLEEP_BETWEEN_UPDATES_SECONDS = float(get_value('GENERAL_SLEEP_BETWEEN_UPDATES_SECONDS', 1/200))
