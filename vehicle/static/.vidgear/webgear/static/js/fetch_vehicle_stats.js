let vehicleStats = null;

function StatsPane(elem) {
    let lastMessage = '';
    function showMessage(message) {
        if (JSON.stringify(message) === lastMessage) {
            return
        }
        elem.innerHTML = '';
        let messageElem = document.createElement('table');
        messageElem.className = 'center'
        for (var key in message) {
            let tr = document.createElement('tr');
            let td = document.createElement('td');
            td.innerText = `${key}`
            tr.appendChild(td)
            td = document.createElement('td');
            td.innerText = `${message[key]}`
            tr.appendChild(td)
            messageElem.appendChild(tr)
        }
        elem.appendChild(messageElem);
        lastMessage = JSON.stringify(message);
    }

    async function fetchStats() {
        let response = await fetch("/stats");
    
        if (response.status == 502) {
        // Status 502 is a connection timeout error,
        // may happen when the connection was pending for too long,
        // and the remote server or a proxy closed it
        // let's reconnect
        await fetchStats();
        } else if (response.status != 200) {
        // An error - let's show it
        showMessage(response.statusText);
        // Reconnect in 200ms
        await new Promise(resolve => setTimeout(resolve, 200));
        await fetchStats();
        } else {
        // Get and show the message
        let message = await response.json();
        vehicleStats = message;
        showMessage(message);
        // Call subscribe() again to get the next message
        await fetchStats();
        }
    }
  
  fetchStats();
}

new StatsPane(document.getElementById('statsPane'));

async function switchSelfDrivingMode() {
    await fetch('/switch-driving-mode', {
        method: 'POST',
        mode: 'same-origin', // no-cors, *cors, same-origin
        cache: 'no-cache',
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
}

async function deactivateEmergencyStop() {
    await fetch('/reset-emergency', {
        method: 'POST',
        mode: 'same-origin', // no-cors, *cors, same-origin
        cache: 'no-cache',
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
}
