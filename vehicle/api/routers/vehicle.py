import shutil
import pathlib
import tempfile

from fastapi import APIRouter, UploadFile, HTTPException
from fastapi.responses import Response

from api.zipper import unzip_files, zip_files, get_folder_names, get_folder_from_name, get_folder_info
from api.ip_helper import get_ip_stats_from_file
from api.docker_logs import get_container_names, get_log_stream_for_container
from api import models

from constants import Config


router = APIRouter()


@router.get("/system/stats", response_model=models.SystemStats)
async def vehicle_system_stats_response():
    """
    Return system stats
    """
    # this might fail, when the updater hasn't run yet. No harm done, in 10 seconds it should work :-)
    system_stats = get_ip_stats_from_file()
    return system_stats


@router.get("/stats", response_model=models.StatusInfo)
async def vehicle_local_stats_response():
    """
    Return vehicle status
    """
    # import inside function enables api startup even when redis is down
    from data_logger.redis_config import get_status_info
    info = get_status_info()
    return info


@router.post("/reset-emergency", response_model=models.StatusInfo)
async def reset_emergency():
    """
    reset Emergency Stop
    """
    # import inside function enables api startup even when redis is down
    from data_logger.redis_config import get_status_info, update_emergency_mode
    update_emergency_mode(True)
    info = get_status_info()
    return info


@router.post("/switch-driving-mode", response_model=models.StatusInfo)
async def switch_driving_mode_endpoint():
    """
    switch driving mode (self-driving vs manual)
    """
    # import inside function enables api startup even when redis is down
    from data_logger.redis_config import get_status_info, switch_driving_mode
    switch_driving_mode()
    info = get_status_info()
    return info


@router.post("/switch-recording", response_model=models.StatusInfo)
async def flip_recording_switch():
    """
    flip recording switch (on/off)
    """
    # import inside function enables api startup even when redis is down
    from data_logger.redis_config import get_status_info, start_stop_recording_switch
    start_stop_recording_switch()
    info = get_status_info()
    return info


@router.get("/recordings/list", response_model=list[models.RecordingFolder])
async def list_recording_folders():
    """
    List of recording folders
    """
    return get_folder_names(exclude_names=['models'])


@router.get("/recordings/download/{path}")
async def download_records_folder(path: str):
    """
    return requested directory
    """
    folder = get_folder_info(get_folder_from_name(path), download_link_base='/api/recordings/download/')
    out_zip_name = f"{folder['name']}.zip"
    zip_data = zip_files(pathlib.Path(folder['path']), folder['name'])
    headers = {'Content-Disposition': 'attachment; filename=%s' % out_zip_name}
    resp = Response(zip_data.getvalue(), media_type="application/x-zip-compressed", headers=headers)
    return resp


@router.delete("/recordings/remove")
async def delete_records_folder(folder: models.RecordingFolder):
    """
    return requested directory
    """
    if str(Config.UPLOADS_BASE_PATH) in str(folder.path):
        shutil.rmtree(folder.path)
        return {"ok": True}
    raise FileNotFoundError(f'{folder.path} is invalid')


@router.get("/models", response_model=list[models.ModelFolder])
async def models_list():
    """
    get list of available models.
    """
    return get_folder_names(Config.UPLOADS_BASE_PATH / 'models', download_link_base='/api/models/download/')


@router.post("/upload-model", response_model=models.StatusInfo)
async def upload_model(file: UploadFile):
    """
    upload model file / zip
    """
    from data_logger.redis_config import get_status_info

    def create_dir(file):
        # FIXME: allow uploading model and/or zip file for model change
        folder_name = pathlib.Path(file.filename).stem
        upload_destination = pathlib.Path(Config.MODEL_FOLDER_PATH) / folder_name
        upload_destination.mkdir(exist_ok=True, parents=True)
        return upload_destination

    await file.seek(0)

    if file.filename.endswith('.zip'):
        with tempfile.NamedTemporaryFile(suffix='.zip') as tmp_file:
            tmp_file.write(await file.read())
            tmp_file.flush()
            tmp_file.seek(0)
            try:
                upload_destination = create_dir(file)
                unzip_files(tmp_file.name, upload_destination)
            except FileNotFoundError as err:
                shutil.rmtree(upload_destination)
                raise HTTPException(status_code=404, detail=str(err))

    elif file.filename.endswith('.onnx'):
        upload_destination = create_dir(file)
        dest_file_path = upload_destination / file.filename
        file.seek(0)
        with open(dest_file_path, 'wb') as dest_file:
            dest_file.write(await file.read())
    else:
        HTTPException(status_code=404, detail="invalid file provided")
    
    info = get_status_info()
    return info


@router.get("/models/download/{path}")
async def download_model_folder(path: str):
    """
    return requested directory
    """
    absolute_path = Config.MODEL_FOLDER_PATH
    folder = get_folder_info(absolute_path / path, download_link_base='/api/models/download/')
    out_zip_name = f"{folder['name']}.zip"
    zip_data = zip_files(pathlib.Path(folder['path']), '.')
    headers = {'Content-Disposition': 'attachment; filename=%s' % out_zip_name}
    resp = Response(zip_data.getvalue(), media_type="application/x-zip-compressed", headers=headers)
    return resp


@router.post("/models/activate", response_model=models.StatusInfo)
async def activate_model(folder: models.ModelFolder):
    """
    activate model for self driving
    """
    from data_logger.redis_config import get_status_info, set_current_selected_model
    
    set_current_selected_model(folder.path)
    
    info = get_status_info()
    return info


@router.delete("/models/remove")
async def delete_model_folder(folder: models.ModelFolder):
    """
    """
    if str(Config.UPLOADS_BASE_PATH / 'models') in str(folder.path):
        shutil.rmtree(folder.path)
        return {"ok": True}
    raise FileNotFoundError(f'{folder.path} is invalid')


@router.get("/containers", response_model=list[models.ContainerInfo])
async def docker_containers():
    """
    Container list with statuses
    """
    return get_container_names()


@router.get("/containers/{container_id}/{since_nano_seconds}", response_model=models.ContainerLog)
async def docker_container_log(container_id: str, since_nano_seconds: float=None):
    """
    Container logs
    """
    if not since_nano_seconds or since_nano_seconds <= 10:
        logs, at_nano_second = get_log_stream_for_container(container_id)
    else:
        logs, at_nano_second = get_log_stream_for_container(container_id, since=float(since_nano_seconds))
    return {
        "logs": logs,
        "since": at_nano_second,
    }
