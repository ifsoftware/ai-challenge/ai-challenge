from api import models

from fastapi import FastAPI
from api.routers import vehicle


# FIXME: Use password or similar to protect from maliciously updating/accessing
# somebody elses car

# app = FastAPI(dependencies=[Depends(get_query_token)])
app = FastAPI(
    openapi_url="/api/openapi.json",
    docs_url="/api/docs",
    redoc_url=None,
    host='',
    servers=[{"url": "/"}],
    root_path_in_servers=False,
)

app.include_router(
    vehicle.router,
    prefix="/api",
    tags=["Vehicle"],
    # dependencies=[Depends(get_token_header)],
    # responses={418: {"description": "I'm a teapot"}},
)


@app.get("/", response_model=models.DummyItem)
async def read_root():
    return {"title": "hello world, nothing to see here"}


