# AI-Challenge

Wichtige Links:

* Notebooks-Repo: [https://gitlab.ost.ch/iza/ai-challenge/notebooks](https://gitlab.ost.ch/iza/ai-challenge/notebooks)
* Präsetation/Kickoff-Repo: [https://gitlab.ost.ch/iza/ai-challenge/presentations](https://gitlab.ost.ch/iza/ai-challenge/presentations)
* Genereller Einstieg: [jetracer.ch](https://jetracer.ch)
* Daten- und Austauschplattform: [nx.jetracer.ch](https://nx.jetracer.ch/)
* Label-Tool: [label.jetracer.ch](https://label.jetracer.ch)
* Trainier- und Aufgaben-Umgebung (GPU-Cluster): [jupyter.jetracer.ch](https://jupyter.jetracer.ch)


## Aufbau

8 Wochen Total

Woche 1: Einführung an der Ost (vor Ort)
Woche 5: Austausch Online, Dateien und Vorschlag von 2 Challenges von den TN selbst
Woche 8: Abschluss Wettbewerb Ost (vor Ort)

### Woche 1: Kickoff an der Ost

Ziele:

* Plattform kennen lernen
* Einrichten lokaler PC/Laptop für Zugang auf GPU-Cluster
	* Einführung ins "Programmieren"
	* Einführung in Machine Learning
	* Jupyter nutzen
	* Cluster Zugang
* erstes Mal fahren (manuell und kleine demo von uns als Beispiel)
* Material mitnehmen nach Hause zum Üben
	* Unterschrift zur Bestätigung


### Woche 2+3: Erstes Modell

* Kamera auslesen und speichern lernen
* Line/Road following

### Woche 4: Online Austausch (W 4)

* Bestimmung Challenges
* ErfA
	* Knoten lösen

### Woche 5-7: Verbesserungen und mehr Inputs

* Verbesserungen des Modells
* Object Detection - Lego Männli, Playmobil
* Edge Detection
* Parkieren?
* Gym mit Hindernissen, Parcours Online
* Parkier-Gym?
* Alles wie Online, muss auf dem Fahrzeug laufen

### Woche 8: Concours an Ost

* Endchallenge, Abschluss

Idee:
* Beat the Ost (Indoor), Foyer Ost, Siegerehrung in Aula
    * Geschicklichkeits-Parkour
    * Collision Avoidance: Hindernisse beachten (don't hit pedestrians)
    * Geschwindikeit, Kurven, Keep on Track


## Dokumentation Hardware

-> Sollen wir diesen Abschnitt ev. in ein separates Readme verschieben? Hier steht jetzt doch vor allem Zeug über das
Projekt als Ganzes.

### Verkabelung

#### Verwendete Pins am Raspi

Die genannten Pinnummern beziehen sich auf folgende Darstellung:

![](img/rc.png)

##### PCA9685-Board

- Pin 1: Strom
- Pin 3: SDA1 I2c
- Pin 5: SCL1 I2c
- Wahlweise Pin 9 oder Pin 6 für Earth / Erdung

##### Receiver

- Pin 33 (GPIO 13): Gear Change (Gangschaltung)
- Pin 35 (GPIO 19): Throttle (Schub / Gas)
- Pin 37 (GPIO 26): Steering (Steuerung)

- Pin 17: Strom für Receiver (Nur 3V3!)
- Pin 20: Earth / Erdung Receiver

Unsere Verkabelung sieht wie folgt aus:
![](img/raspberry_board_2022-06-20.jpeg)

#### Verwendete Pins an den Geräten

##### Receiver

Auf dem Receiver entspricht: 

- Channel 1: Steering (Steuerung)
- Channel 2: Throttle (Schub / Gas)
- Channel 4: Gear Change (Gangschaltung)

Die Stromkabel können bei einem beliebigen Channel eingesteckt werden. Wichtig ist nur, dass:
- Live in der mittleren Reihe eingesteckt wird
- Earth in der obersten Reihe eingesteckt wird

**ACHTUNG:** Die Pins sollten nicht umgesteckt werden (beispielsweise Verbindung vom Receiver zum Raspi umstecken auf 
Receiver zu Motor), solange noch Pins mit dem Raspi verbunden sind. Der Raspi verträgt das nicht. Höchstwahrscheinlich
sollte man auch den Receiver nicht mit Strom vom Motor speisen, während er noch in irgendeiner Art und Weise mit dem
Raspi verbunden ist. Also: Receiver entweder _nur_ am Motor oder _nur_ mit dem Raspi verbinden und _nicht_ umstecken, 
solange noch Verbindungen zum Raspi bestehen; erst ausstecken.

Am Receiver sieht das dann wie folgt aus (vgl. Kabelfarben mit oberem Bild):
![](img/receiver_2022-06-20.jpeg)

##### PCA9685-Board

Am PCA9685-Board werden die Pins wie folgt eingesteckt:

![](img/pca9685_board_2022-06-20.jpeg)

Die Pins, die zum Raspi führen, stecken seitwärts links am Board. Sie sind neben den Pins auch angeschrieben, falls hier Verwirrung entstehen sollte.

TODO: Document other PCA9685 Pins.

## Steuerung abgreifen

### Software

Die Werte werden mittels Pulse Width Modulation (PWM) übertragen.

Im `dev/vehicle`-Ordner dieses Repos befinden sich die benötigten Klassen, um die Steuerung abzugreifen. Die Werte 
werden mittels pigpio über die obig bestimmten GPIO-Pins am Raspi abgerufen. Ausgelesen können sie mit der 
`RCReceiver`-Klasse. 

Im Config-File `constants.py` können einige Werte angepasst werden, beispielsweise die verwendeten GPIO-Pins. Sie 
sollten aber standardmässig bereits auf die korrekten Werte gesetzt sein.

Mit dieser Library wird im Hintergrund pigpio verwendet, um auf die Pins zuzugreifen. Es muss sichergestellt werden, 
dass pigpio auf dem System läuft. Am besten lässt man pigpiod automatisch starten. Auf dem ersten JetRacer gab es damit
einige Schwierigkeiten. Der Service kann auch manuell mit `sudo pigpiod` gestartet werden, was man auf dem Probemodell
momentan auch machen muss.

Man kann die Werte vom RCReceiver-Objekt mit `receiver.run()` abrufen (`receiver` ist hier einfach der Name der Variable).
Das gibt ein Array mit Werten zurück. 
* Auf `result[0]` findet man den Steering-Wert
* Auf `result[1]` befindet sich der Throttle-Wert
* Auf `result[2]` befindet sich der aktuell eingestellte Gang

(`result` ist wieder einfach eine Variable)

## Fahrzeug ansteuern

Das Fahrzeug kann mit der `Vehicle`-Klasse gesteuert werden. Es hat folgende Funktionen:

* `set_steering`: setzt die Lenkung
* `set_throttle`: setzt den Schub / die Schubkraft
* `set_gear`: setzt den Gang

Alle diese Klassen nehmen direkt die Werte aus der `RCReceiver`-Klasse entgegen (bzw. bei Steering haben wir momentan
noch eine Ausgabe von ±1.2, welche im Moment einfach auf ±1 beschränkt wird).

Bei `set_throttle` bedeutet ein Wert von -1 "voll rückwärts" und ein Wert von 1 "voll vorwärts". 
Bei `set_steering` ist ein Wert von -1 "voll nach links" und ein Wert von 1 "voll nach rechts".

Natürlich können diese Werte abgestuft sein, sprich es sind Kommazahlen und alle Werte dazwischen sind auch gültig.

Bei `set_gear` wird – anders als bei den anderen Funktionen – ein Wert von entweder 0 oder 1 erwartet. 0 ist dabei der 
langsame Gang, 1 der schnelle.

Um die Verwendung dieser Klassen und Funktionen zu sehen, kann der Code im Ordner `dev/vehicle` angeschaut werden.

### Korrekturen

Es kann sein, dass bei der Steuerung der Wert 0 die Lenkung nicht gerade ausrichtet. Somit würde der JetRacer eine 
leichte Kurve fahren, wenn man keine Lenkeingaben macht. In der Datei `constants.py` befindet sich eine Klasse mit 
ein paar Parametern, darunter auch unter `# for Vehicle` `STEERING_OFFSET`. Um die neutrale Richtung anzupassen, kann
etwas mit diesem Wert gespielt werden. Gewöhnlicherweise sollten kleinere Zahlen von ca. ±15 reichen. Der Wert sollte 
45 nicht übersteigen, da ansonsten die verwendete Library einen Error werfen könnte.

Diese Zahl wird vermutlich je Fahrzeug unterschiedlich hoch / tief ausfallen und sollte daher individuell eingerichtet 
werden.
