# syntax=docker/dockerfile:1

# platform option can be removed if necessary, is needed to trigger Rosetta on M1 Macs (doesn't run properly on ARM)
FROM --platform=linux/amd64 ubuntu:22.04

ARG PROJECT_NAME='ai-challenge'
ARG PROJECT_DIR='/app'
ARG USER_ID=999

ENV PYTHONUNBUFFERED=rununbuffered \
    PYTHONIOENCODING=utf-8 \
    SHELL=/bin/bash \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    MAX_POETRY_VERSION=2 \
    DOCKERIZE_VERSION=v0.6.1 \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    python-is-python3 \
    wget \
    locales \
    libmagic1 \
    ffmpeg \
    gpac \
    unzip \
  && rm -rf /var/lib/apt/lists/* \
  && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
  && locale-gen

RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  # Enable prompt color in the skeleton .bashrc before creating the default USERNAME
  && sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc

RUN pip install -U "poetry<$MAX_POETRY_VERSION" \
  && poetry config virtualenvs.create false

ENV WORKDIR=${PROJECT_DIR}/${PROJECT_NAME}

WORKDIR ${PROJECT_DIR}

ENV USER_ID=${USER_ID}
COPY ./poetry.lock ./pyproject.toml ${PROJECT_DIR}/
RUN poetry install --no-interaction --no-ansi

COPY . ${PROJECT_DIR}/

WORKDIR ${WORKDIR}

USER ${USER_ID}
