FROM node:16 as development

RUN yarn global add @quasar/cli

WORKDIR /app

RUN chown -R "1000:1000" /app
USER "1000"


FROM node:16 as builder

WORKDIR /builder

RUN yarn global add @quasar/cli

COPY ./package.json ./yarn.lock /builder/
RUN yarn install --dev
COPY . /builder
RUN quasar build
RUN ls -lah /builder/dist/spa

# TODO: Production

FROM geometalab/env-configurable-caddy as production

WORKDIR /var/www/html

COPY --from=builder /builder/dist/spa /var/www/html

RUN ls -lah /var/www/html
