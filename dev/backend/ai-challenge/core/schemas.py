from __future__ import annotations
from typing import List

from ninja import Schema

from django.contrib.auth.models import Group

from ninja import ModelSchema
from django.contrib.auth import get_user_model
from core.models import Profile

User = get_user_model()


class LanguageCode(Schema):
    language_code: str


class Language(Schema):
    language_code: str
    language: str


class UploadResponseSchema(Schema):
    status_code: int
    message: str


class DirContentSchema(Schema):
    path: str
    name: str
    is_dir: bool
    size: int
    last_modified: str


class PiRegistrationSchema(Schema):
    ip_addr: str
    hostname: str


class PiResponseSchema(Schema):
    pi_id: int
    ip_addr: str
    hostname: str
    group: str = ''


class NerdstatRam(Schema):
    used: int
    total: int
    percent_used: float


class NerdstatStorage(Schema):
    used: int
    total: int


class NerdstatSchema(Schema):
    hostname: str
    ip_addr: str
    cpu_usage: list[float]
    ram: NerdstatRam
    storage: NerdstatStorage
    model_name: str


class GroupSchema(ModelSchema):
    class Config:
        model = Group
        model_fields = ['id', 'name']


class ProfileSchema(ModelSchema):
    class Config:
        model = Profile
        model_fields = ['id', 'preferred_language']


class UserSchema(ModelSchema):
    groups: List[GroupSchema] = []
    preferences: ProfileSchema = None

    class Config:
        model = User
        model_fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'is_superuser',
        ]
        # model_fields = "__all__"
        # model_exclude = ['password', 'last_login', 'user_permissions']
