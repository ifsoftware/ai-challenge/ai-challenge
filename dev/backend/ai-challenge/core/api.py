from datetime import timedelta

from ninja import Router
import requests
import json
from types import SimpleNamespace

from allauth.socialaccount.models import SocialToken, SocialApp
from django.conf import settings
from django.contrib.auth.models import Group
from django.http import HttpResponse
from django.utils import translation, timezone
from django.core.exceptions import ObjectDoesNotExist

from .schemas import Language, LanguageCode
from .exceptions import TokenRefreshError, RequestFailedError

router = Router()

LANGUAGES_DICT_LIST = [
    {
        'language': t[1],
        'language_code': t[0],
    }
    for t in settings.LANGUAGES
]

from typing import List
from ninja import Router, File, Form
from ninja.files import UploadedFile
from .models import User, Raspi
from .schemas import UserSchema, UploadResponseSchema, DirContentSchema, PiRegistrationSchema, PiResponseSchema, NerdstatSchema
from ninja.security import django_auth
import xmltodict

router = Router()
unprotected_router = Router()

NEXTCLOUD_BASE_URL = 'https://nx.jetracer.ch/'

NEXTCLOUD_TOKEN_ENDPOINT = 'apps/oauth2/api/v1/token'
NEXTCLOUD_USER_API = '/ocs/v1.php/cloud'
NEXTCLOUD_FILES = '/remote.php/dav/files'


def decode_json(text: str):
    return json.loads(text, object_hook=lambda d: SimpleNamespace(**d))


def get_nextcloud_api_token(user):
    social_token = SocialToken.objects.get(account__user=user, account__provider='nextcloud')

    if social_token.expires_at < timezone.now():
        return refresh_oauth_token(social_token)

    return social_token.token


def refresh_oauth_token(social_token: SocialToken):

    def token_updater(token):
        social_token.token = token.access_token
        social_token.token_secret = token.refresh_token
        social_token.expires_at = timezone.now() + timedelta(seconds=int(token.expires_in))
        social_token.save()

        return token.access_token

    oauth_credentials = SocialApp.objects.get(name='NextCloud')

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    request_body = {
        'grant_type': 'refresh_token',
        'refresh_token': social_token.token_secret,
        'client_id': oauth_credentials.client_id,
        'client_secret': oauth_credentials.secret,
    }

    response = requests.post(url=NEXTCLOUD_BASE_URL + NEXTCLOUD_TOKEN_ENDPOINT,
                             headers=headers, data=request_body, auth=None)

    if response.status_code != 200:
        raise TokenRefreshError(response.text)

    new_token = token_updater(decode_json(response.text))

    return new_token


def retrieve_or_create_group(group_name: str):
    try:
        group = Group.objects.get(name=group_name)
    except ObjectDoesNotExist:
        group = Group(name=group_name)
        group.save()

    return group

@router.get('/users', response=List[UserSchema])
def event_details(request):
    return User.objects.all()


@router.get('/users/{user_id}', response=UserSchema)
def event_details(request, user_id: int):
    return User.objects.get(id=user_id)


@router.get('/nextcloud/user-groups', auth=django_auth, response=List[str])
def nx_user_groups(request):
    current_user = request.user

    try:
        api_token = get_nextcloud_api_token(current_user)
    except ObjectDoesNotExist:
        error_response = HttpResponse('User does not use NextCloud Social Login.', content_type='text/plain')
        error_response.status_code = 405

        return error_response
    except TokenRefreshError:
        error_response = HttpResponse('NextCloud Token could not be refreshed. Try logging off and on again.',
                                      content_type='text/plain')
        error_response.status_code = 403

        return error_response

    headers = {
        'OCS-APIRequest': 'true',
        'Authorization': f'Bearer {api_token}'
    }

    payload = {
        'format': 'json',
    }

    response = requests.get(url=f'{NEXTCLOUD_BASE_URL + NEXTCLOUD_USER_API}/users/{current_user.email}',
                            headers=headers, params=payload)

    if response.status_code == 200:
        user_info = decode_json(response.text)
        user_groups = user_info.ocs.data.groups

        for group_name in user_groups:
            # could perhaps be faster if we query for all names simultaneously
            group = retrieve_or_create_group(group_name)

            current_user.groups.add(group)
            current_user.save()

        return user_groups

    else:
        error_response = HttpResponse(response.text, content_type='application/json')
        error_response.status_code = response.status_code

        return error_response


@router.get('nextcloud/files/list', response=List[DirContentSchema])
def nx_file_list(request, dir_path: str = '/', raw_url: bool = False):
    current_user = request.user

    try:
        api_token = get_nextcloud_api_token(current_user)
    except ObjectDoesNotExist:
        error_response = HttpResponse('User does not have access to NextCloud files as they do not use NextCloud Social'
                                      ' Login.', content_type='text/plain')
        error_response.status_code = 405

        return error_response
    except TokenRefreshError:
        error_response = HttpResponse('NextCloud Token could not be refreshed. Try logging off and on again.',
                                      content_type='text/plain')
        error_response.status_code = 403

        return error_response

    headers = {
        'Authorization': f'Bearer {api_token}'
    }

    # all available file props: https://docs.nextcloud.com/server/latest/developer_manual/client_apis/WebDAV/basic.html
    body = '''<?xml version="1.0" encoding="utf-8" ?>
    <d:propfind xmlns:d="DAV:">
    <d:prop xmlns:oc="http://owncloud.org/ns">
      <d:getlastmodified/>
      <oc:size/> 
    </d:prop>
    </d:propfind>'''

    if raw_url:
        url = NEXTCLOUD_BASE_URL + dir_path
    else:
        url = NEXTCLOUD_BASE_URL + NEXTCLOUD_FILES + f'/{current_user.email}' + dir_path

    response = requests.request(method='PROPFIND',
                                url=url,
                                headers=headers, data=body)

    if response.status_code == 207:  # 207 = Multi-Status
        response_dict = xmltodict.parse(response.text)

        dir_contents = []

        for entry in response_dict['d:multistatus']['d:response']:
            path_component = entry['d:href'].split('/')

            dir_content = {
                'path': entry['d:href'],
                # Name still struggles with special characters (they are url-encoded).
                # Name is not a supported parameter for the xml request, hence this workaround.
                'name': path_component[-1] if path_component[-1] != '' else path_component[-2],
                'is_dir': entry['d:href'][-1] == '/',
                'size': entry['d:propstat']['d:prop']['oc:size'],
                'last_modified': entry['d:propstat']['d:prop']['d:getlastmodified'],
            }

            dir_contents.append(dir_content)

        return dir_contents
    else:
        # left error response as raw xml as its purpose is mainly debugging.
        error_response = HttpResponse(response.text, content_type='application/xml')
        error_response.status_code = response.status_code

        return error_response


@router.post('nextcloud/files/upload', auth=django_auth, response=List[UploadResponseSchema])
def nx_file_upload(request, files: List[UploadedFile], dir_path: str = '', raw_url: bool = True):
    current_user = request.user

    try:
        api_token = get_nextcloud_api_token(current_user)
    except ObjectDoesNotExist:
        error_response = HttpResponse('User cannot upload files to NextCloud as they do not use NextCloud Social'
                                      ' Login.', content_type='text/plain')
        error_response.status_code = 405

        return error_response
    except TokenRefreshError:
        error_response = HttpResponse('NextCloud Token could not be refreshed. Try logging off and on again.',
                                      content_type='text/plain')
        error_response.status_code = 403

        return error_response

    if not files:
        error_response = HttpResponse('Bad Request: No files provided.', content_type='text/plain')
        error_response.status_code = 400

        return error_response

    if raw_url:
        url = NEXTCLOUD_BASE_URL + dir_path
    else:
        url = NEXTCLOUD_BASE_URL + NEXTCLOUD_FILES + f'/{current_user.email}' + dir_path

    session = requests.session()
    session.headers = {
        'Authorization': f'Bearer {api_token}',
    }

    responses = []

    for file in files:
        file_url = url + f'/{file.name}'
        response = session.put(url=file_url, data=file.read())

        dict_response = {
            'status_code': response.status_code,
            'message': response.text,
        }

        responses.append(dict_response)

    return responses


@router.get('nextcloud/files/download', auth=django_auth)
def nx_file_download(request, file_path: str = '/', raw_url: bool = False):
    current_user = request.user

    try:
        api_token = get_nextcloud_api_token(current_user)
    except ObjectDoesNotExist:
        error_response = HttpResponse('User does not have access to NextCloud files as they do not use NextCloud Social'
                                      ' Login.', content_type='text/plain')
        error_response.status_code = 405

        return error_response
    except TokenRefreshError:
        error_response = HttpResponse('NextCloud Token could not be refreshed. Try logging off and on again.',
                                      content_type='text/plain')
        error_response.status_code = 403

        return error_response

    headers = {
        'Authorization': f'Bearer {api_token}'
    }

    if raw_url:
        url = NEXTCLOUD_BASE_URL + file_path
    else:
        url = NEXTCLOUD_BASE_URL + NEXTCLOUD_FILES + f'/{current_user.email}' + file_path

    response = requests.get(url=url, headers=headers)

    if response.status_code == 200:
        response_headers = {
            'Content-Type': response.headers.get('Content-Type'),
            'Content-Disposition': response.headers.get('Content-Disposition'),
        }

        return HttpResponse(content=response.content, headers=response_headers)
    else:
        # left error response as raw xml as its purpose is mainly debugging.
        error_response = HttpResponse(response.text, content_type='application/xml')
        error_response.status_code = response.status_code

        return error_response


# Unprotected router needed, as you cannot have django_auth and csrf=False. But this cannot be accessed by the raspi if
# csrf is enabled.
@unprotected_router.post('/pi/register', response=PiResponseSchema)
def register_new_pi(request, registration: PiRegistrationSchema):
    raspi = Raspi(ip_addr=registration.ip_addr, hostname=registration.hostname)

    group = retrieve_or_create_group(registration.hostname)

    raspi.group = group

    raspi.save()

    response_json = {
        'pi_id': raspi.pk,
        'ip_addr': raspi.ip_addr,
        'hostname': raspi.hostname,
    }

    return response_json


@unprotected_router.post('/pi/register/{pi_id}', response=PiResponseSchema)
def register_pi(request, registration: PiRegistrationSchema, pi_id: int):
    try:
        raspi = Raspi.objects.get(pk=pi_id)
    except ObjectDoesNotExist:
        return HttpResponse(f'Raspberry with id {pi_id} has not been registered yet.', status=400)

    raspi.hostname = registration.hostname
    raspi.ip_addr = registration.ip_addr

    # group reassignment not needed, as the group already exists, since at the latest point it gets created when the
    # raspi registers itself. Groups / Groupnames from nextcloud should match with hostnames from pis, so they
    # automatically get assigned to the correct group.

    raspi.save()

    response_json = {
        'pi_id': raspi.pk,
        'ip_addr': raspi.ip_addr,
        'hostname': raspi.hostname,
    }

    return response_json


@router.get('/pi', response=List[PiResponseSchema], auth=django_auth)
def get_pis(request):
    raspis = Raspi.objects.all()

    if not raspis:
        return []

    response_jsons = [
        {
            'pi_id': raspi.pk,
            'ip_addr': raspi.ip_addr,
            'hostname': raspi.hostname,
            'group': raspi.group.name if raspi.group else '',
        }
        for raspi in raspis
    ]

    return response_jsons


def retrieve_vehicle_stats(ip_addr: str):
    response = requests.get(f'http://{ip_addr}/stats', timeout=15)

    if response.status_code != 200:
        raise RequestFailedError(response.text)

    response_obj = decode_json(response.text)

    model_name_response = requests.get(f'http://{ip_addr}/model', timeout=15)

    model_obj = None

    if model_name_response.status_code == 200:
        model_obj = decode_json(model_name_response.text)

    nerdstats = {
        'hostname': response_obj.hostStart.hostname,
        'ip_addr': ip_addr,
        'cpu_usage': [
            cpu_percent
            for cpu_percent in response_obj.cpu
        ],
        'ram': {
            'used': response_obj.ram.used,
            'total': response_obj.ram.total,
            'percent_used': response_obj.ram.usedPercent,
        },
        'storage': {
            'used': response_obj.storage.used,
            'total': response_obj.storage.total
        },
        'model_name': model_obj.file if model_obj else 'No Model'
    }

    return nerdstats


@router.get('/pi/stats', response=NerdstatSchema)
def get_nerdstats(request):
    groups = request.user.groups

    raspi = None

    for group in groups.all():
        try:
            raspi = Raspi.objects.get(group=group)
            break
        except ObjectDoesNotExist:
            continue

    if raspi is None:
        return HttpResponse('No JetRacer assigned to your group.', status=404)

    try:
        nerdstats = retrieve_vehicle_stats(raspi.ip_addr)
    except TimeoutError:
        return HttpResponse('The JetRacer is unreachable', status=408)
    except RequestFailedError:
        return HttpResponse('Nerdstats could not be retrieved due to an unknown error', status=500)

    return nerdstats


@router.get('/pi/stats/{id}', response=NerdstatSchema)
def get_nerdstats_for_pi(request, id: int):
    try:
        raspi = Raspi.objects.get(pk=id)
    except ObjectDoesNotExist:
        return HttpResponse('Pi Not Found', status=404)

    try:
        nerdstats = retrieve_vehicle_stats(raspi.ip_addr)
    except TimeoutError:
        return HttpResponse('The JetRacer is unreachable', status=408)
    except RequestFailedError:
        return HttpResponse('Nerdstats could not be retrieved due to an unknown error', status=500)

    return nerdstats


@router.get('/pi/{pi_id}', response=PiResponseSchema, auth=django_auth)
def get_pi(request, pi_id: int):
    try:
        raspi = Raspi.objects.get(pk=pi_id)
    except ObjectDoesNotExist:
        return HttpResponse(f'Raspberry with id "{pi_id}" does not exist', status=404)

    response_json = {
        'pi_id': raspi.pk,
        'ip_addr': raspi.ip_addr,
        'hostname': raspi.hostname,
        'group': raspi.group.name if raspi.group else '',
    }

    return response_json


@router.post('pi/{pi_id}/assigngroup', response=PiResponseSchema, auth=django_auth)
def assign_pi_to_group(request, pi_id: int, group_name: str = Form('')):
    try:
        raspi = Raspi.objects.get(pk=pi_id)

        if group_name != '':
            group = Group.objects.get(name=group_name)
        else:
            group = None

    except ObjectDoesNotExist:
        return HttpResponse(f'Requested Group not found ({group_name})', status=400)

    raspi.group = group
    raspi.save()

    response_json = {
        'pi_id': raspi.pk,
        'ip_addr': raspi.ip_addr,
        'hostname': raspi.hostname,
        'group': raspi.group.name if raspi.group else '',
    }

    return response_json

@router.get('/i18n/languages/', response=List[Language])
def languages(request):
    languages = LANGUAGES_DICT_LIST
    return languages


@router.get('/i18n/languages/current', response=Language)
def languages(request):
    cur_language = translation.get_language()
    cur_language = [
        l
        for l in LANGUAGES_DICT_LIST
        if cur_language == l.get('language_code')
    ][0]
    return cur_language


@router.post('/i18n/languages/')
def set_language(request, user_language: LanguageCode):
    language_code = user_language.dict().get('language_code')
    translation.activate(language_code)
    response = HttpResponse()
    response.set_cookie(settings.LANGUAGE_COOKIE_NAME, language_code)
    return response
