# Generated by Django 4.0.5 on 2022-10-21 12:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_raspi'),
    ]

    operations = [
        migrations.AddField(
            model_name='raspi',
            name='hostname',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
