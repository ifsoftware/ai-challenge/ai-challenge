class TokenRefreshError(Exception):
    pass


class RequestFailedError(Exception):
    pass
