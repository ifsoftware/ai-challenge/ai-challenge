from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import AbstractUser, Group


class User(AbstractUser):
    pass

    def preferences(self):
        try:
            return self.profile
        except Profile.DoesNotExist:
            return None


class Profile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('user'),
        related_name='profile',
        on_delete=models.CASCADE,
    )
    preferred_language = models.CharField(
        choices=settings.LANGUAGES,
        verbose_name=_('preferred language'),
        max_length=100,
    )


class Raspi(models.Model):  # ID is generated automatically
    group = models.OneToOneField(Group, null=True, blank=True, on_delete=models.SET_NULL)
    hostname = models.CharField(max_length=100, null=False)
    ip_addr = models.CharField(max_length=20, null=False)
