import json
from typing import Any, Mapping, Type
import orjson

from ninja.parser import Parser
from ninja.renderers import BaseRenderer
from ninja.responses import NinjaJSONEncoder


class ORJSONParser(Parser):
    def parse_body(self, request):
        return orjson.loads(request.body)


class ORJSONRenderer(BaseRenderer):
    media_type = 'application/json'

    def render(self, request, data, *, response_status):
        return orjson.dumps(data)


class DebugRenderer(BaseRenderer):
    json_dumps_params: Mapping[str, Any] = {}
    encoder_class: Type[json.JSONEncoder] = NinjaJSONEncoder

    def render(self, request, data, *, response_status):
        json_allowed = request.accepts('application/json')
        html_allowed = request.accepts('text/html')

        if html_allowed or not json_allowed:

            self.media_type = 'text/html'
            from django.shortcuts import render

            data = json.dumps(
                data,
                cls=self.encoder_class,
                indent=4,
                sort_keys=True,
                **self.json_dumps_params
            )
            return render(
                request,
                template_name='core/debug_render.html',
                context={'data': data},
            )
        else:
            self.media_type = 'application/json'
            return orjson.dumps(data)
