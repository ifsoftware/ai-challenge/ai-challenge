from ninja import NinjaAPI
from core.api import router as core_router
from core.api import unprotected_router

api = NinjaAPI(csrf=True)

api.add_router("", core_router)

unprotected_api = NinjaAPI(urls_namespace='unprotected', csrf=False)

unprotected_api.add_router("/de6d6da0-8aac-4a52-9cc1-809dc67f55f5/", unprotected_router)
