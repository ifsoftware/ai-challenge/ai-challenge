from pathlib import Path
import environ

env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('DJANGO_SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DJANGO_DEBUG')

ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=[])
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
USE_X_FORWARDED_HOST = env.bool('DJANGO_USE_X_FORWARDED_HOST', True)
USE_X_FORWARDED_PORT = env.bool('DJANGO_USE_X_FORWARDED_PORT', True)
CSRF_TRUSTED_ORIGINS = env.list('DJANGO_CSRF_TRUSTED_ORIGINS', default=[])

# Application definition

INSTALLED_APPS = [
    # must become before "django.contrib.admin",
    # 'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    # allauth authentication
    'allauth',
    'allauth.account',
    'allauth.socialaccount',

    # uncomment and follow https://django-allauth.readthedocs.io/en/latest/installation.html
    # to enable other backends
    'allauth.socialaccount.providers.nextcloud',
    # 'allauth.socialaccount.providers.gitea',
    # 'allauth.socialaccount.providers.github',
    # 'allauth.socialaccount.providers.gitlab',
    # 'allauth.socialaccount.providers.google',
    # 'allauth.socialaccount.providers.openstreetmap',
    # 'allauth.socialaccount.providers.twitter',

    # third party
    'django_extensions',
    'debug_toolbar',
    # 'django_celery_results',
    # own apps
    'core',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    # read os.environ['DATABASE_URL'] and raises
    # ImproperlyConfigured exception if not found
    #
    # The db() method is an alias for db_url().
    'default': env.db('DJANGO_DATABASE_URL'),
}

CELERY_BROKER_URL = env.str(
    'CELERY_BROKER_URL', default='pyamqp://guest@rabbitmq//'
)
CELERY_RESULT_BACKEND = env.str('CELERY_RESULT_BACKEND', default='django-db')
CELERY_CACHE_BACKEND = env.str('CELERY_CACHE_BACKEND', default='default')
CELERY_TIMEZONE = env.str('CELERY_TIMEZONE', default='Europe/Zurich')
CELERY_TASK_TRACK_STARTED = env.bool('CELERY_TASK_TRACK_STARTED', default=True)
CELERY_TASK_TIME_LIMIT = env.int(
    'CELERY_TASK_TIME_LIMIT', default=30 * 60
)  # in seconds

CACHES = {
    # Read os.environ['CACHE_URL'] and raises
    # ImproperlyConfigured exception if not found.
    #
    # The cache() method is an alias for cache_url().
    'default': env.cache('DJANGO_CACHE_URL'),
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'core.User'

AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
]

SOCIALACCOUNT_PROVIDERS = {
    'nextcloud': {
        'SERVER': 'https://nx.jetracer.ch',
    }
}

SOCIALACCOUNT_STORE_TOKENS = True

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('de', 'Deutsch'),
    ('en', 'English'),
)
MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TIME_ZONE = 'Europe/Zurich'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = env.str('DJANGO_STATIC_URL', '/static/')
STATIC_ROOT = env.str('DJANGO_STATIC_ROOT', '/data/static')
MEDIA_URL = env.str('DJANGO_MEDIA_URL', '/uploads/')
MEDIA_ROOT = env.str('DJANGO_MEDIA_ROOT', '/data/uploads')

PRIVATE_ROOT = env.str('DJANGO_PRIVATE_ROOT', '/data/private')
PRIVATE_URL = env.str('DJANGO_PRIVATE_URL', '/api/v1/private/')

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

EMAIL_CONFIG = env.email(
    'DJANGO_EMAIL_URL',
)
vars().update(EMAIL_CONFIG)

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
}
INTERNAL_IPS = env.list('DJANGO_INTERNAL_IPS', default=[])
VERIFICATION_FROM_EMAIL = env.str(
    'DJANGO_VERIFICATION_FROM_EMAIL', default='test@example.org'
)
FRONTEND_URL = env.str('DJANGO_FRONTEND_URL', default='http://localhost:8080')
REST_REGISTRATION = {
    'REGISTER_VERIFICATION_URL': f'{FRONTEND_URL}/verify-user/',
    'RESET_PASSWORD_VERIFICATION_URL': f'{FRONTEND_URL}/reset-password/',
    'REGISTER_EMAIL_VERIFICATION_URL': f'{FRONTEND_URL}/verify-email/',
    'VERIFICATION_FROM_EMAIL': VERIFICATION_FROM_EMAIL,
}

SITE_ID = 1

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda request: DEBUG,
}

API_PREFIX = env.str('API_PREFIX', default='api')
API_PREFIX = API_PREFIX.rstrip('/').lstrip('/')
LOGIN_REDIRECT_URL = '/#/loginredirect'
LOGIN_URL = f'/{API_PREFIX}/accounts/login/'
LOGOUT_REDIRECT_URL = '/'

# Storage
# maybe consider using https://docs.djangoproject.com/en/4.0/ref/contrib/staticfiles/#manifeststaticfilesstorage
# STATICFILES_STORAGE = ''
