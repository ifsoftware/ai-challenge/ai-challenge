#!/bin/bash

set -e

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd ${SCRIPTPATH}/..
docker compose -f compose.build.yml build --pull 
docker compose -f compose.build.yml push
