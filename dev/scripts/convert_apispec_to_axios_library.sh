#!/bin/bash

set -e

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"


wget https://localhost:8080/api/openapi.json --no-check-certificate --header='accept: application/json' -O ${SCRIPTPATH}/schema.json

# docker run -v"${PWD}:/local" --rm -it openapitools/openapi-generator-cli generate -i /local/schema.json -g typescript-axios -o /local/generated-axios
docker run --user "1000:1000" -v "${SCRIPTPATH}:/local" --rm -it openapitools/openapi-generator-cli generate --skip-validate-spec --model-name-prefix I -i /local/schema.json -g typescript-axios -o /local/generated-axios-client
# requires '--skip-validate-spec' because of endpoints /api/core/users/{user_id} and /api/core/i18n/languages/current.
# their 'operationId' is not unique / got repeated.

rm -rf ${SCRIPTPATH}/generated-axios-client/.openapi-generator
rm -rf ${SCRIPTPATH}/generated-axios-client/.gitignore
rm -rf ${SCRIPTPATH}/generated-axios-client/.npmignore
rm -rf ${SCRIPTPATH}/generated-axios-client/.openapi-generator-ignore
rm -rf ${SCRIPTPATH}/generated-axios-client/git_push.sh

rm -rf ${SCRIPTPATH}/../frontend/ai-challenge/src/lib/api/generated-axios-client
mv ${SCRIPTPATH}/generated-axios-client ${SCRIPTPATH}/../frontend/ai-challenge/src/lib/api/

rm -f ${SCRIPTPATH}/schema.json
