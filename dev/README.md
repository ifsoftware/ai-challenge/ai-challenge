# AI-Challenge

-> Django with Quasar.

Even though in reality, there is much more to it than just using a ai-challenge to detect and see a quasar, the metaphor sounded too good to not to be used.

## Usage

Currently, only forking is supported. Clone the repository and start from there.

### Local development

Prerequisites:

* docker (TODO: link this)
* docker compose plugin (TODO: link this)

#### Startup

```bash
git clone ... # TBD
cd ai-challenge
docker compose build --pull
docker compose up
# use ctrl+c to stop
```

#### Initial setup

create an admin user:

```bash
docker compose run --rm --entrypoint="" backend bash -c './manage.py createsuperuser'
```

change default site (accept insecure certificate!):

1. Login to https://localhost:8080/api/admin/
1. change the default site to `localhost:8080`: https://localhost:8080/api/admin/sites/site/1/change/
1. Add gitlab login (or change it to something different in settings.py)
    1. create api token in gitlab.com, reversed url: `https://localhost:8080/api/accounts/gitlab/login/callback/`
    1. add https://localhost:8080/api/admin/socialaccount/socialapp/add/ using the token/key
