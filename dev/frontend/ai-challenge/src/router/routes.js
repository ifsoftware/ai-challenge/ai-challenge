
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: "hub", path: '', component: () => import('pages/HubDashboard.vue')},
      { name: "cutter", path: 'cutter', component: () => import('pages/CutterPage.vue')},
      { name: "donkey", path: 'donkey', component: () => import('pages/IndexPage.vue') },
      { name: "loginredirect", path: "loginredirect", component: () => import('pages/LoginRedirect.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
