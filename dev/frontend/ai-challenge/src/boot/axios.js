import { boot } from 'quasar/wrappers'
import axios from 'axios'
import { useUserStore } from 'stores/user'
// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN"
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';

const api = axios.create({ baseURL: '/api/', withCredentials: true })

// TODO: Move to better place
api.interceptors.request.use((config) => {
  const userStore = useUserStore();

  if (userStore.accessToken) {
    config.headers.Authorization = `Bearer ${userStore.accessToken}`;
  }
  return config;
},
(error) => {
  return Promise.reject(error);
}
);

api.interceptors.response.use(
(res) => {
  return res;
},
async (err) => {
  const originalConfig = err.config;
    // Access Token was expired
    if (err.response?.status === 401 && !originalConfig._retry) {
      originalConfig._retry = true;
      try {
        const userStore = useUserStore();
        const rs = await api.post('token/refresh', {
          refresh: userStore.refreshToken,
        });
        const { access } = rs.data;
        userStore.accessToken.value = access;
        originalConfig.headers.Authorization = `Bearer ${userStore.accessToken}`;
        return axiosInstance(originalConfig);
      } catch (_error) {
        return Promise.reject(_error);
      }
  }
  return Promise.reject(err);
});


export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)

})

export { api }
