import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { ref } from 'vue';

export const usePiStore = defineStore('pi', () => {

  async function getAllPis(){
    return await api.get('pi')
  }

  async function getPi(pi){
    return await api.get('pi/'+ pi);
  }

  async function getStats(pi){
    if (pi === undefined) {
      return await api.get('/pi/stats');
    }
    return await api.get('pi/stats/' + pi)
  }

  async function getModel(pi){
    return await api.get('pi/model/' + pi)
  }

  async function getUploads(pi){
    return await api.get('pi/uploads' + pi)
  }

  return {
    getPi,
    getAllPis,
    getStats,
    getModel,
    getUploads,
  }
})

export const useNextCloudStore = defineStore('nextcloud', () => {

  function getFiles(path){
    return api.get('nextcloud/files/list');
  }

  function getUserGroups(){
    return api.get('nextcloud/user-groups');
  }

  function getUsers(){
    return api.get('users');
  }

  function getUser(userid){
    return api.get('users/' + userid)
  }

  return { getFiles, getUser, getUserGroups, getUsers }
})


