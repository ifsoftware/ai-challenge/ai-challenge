import { defineStore } from 'pinia';
import { api } from 'boot/axios';

export const useUserStore = defineStore('users', {
  state: () => ({
    users: [],
    loggedInUser: null,
  }),

  getters: {
    userList (state) {
      return state.users
    }
  },

  actions: {
    async fetchUsers (state) {
      state.users = await api.get('core/users')
    }
  }
})
