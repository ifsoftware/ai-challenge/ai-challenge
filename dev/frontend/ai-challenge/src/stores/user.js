import { defineStore } from 'pinia';
import { api } from 'boot/axios';
import { ref } from 'vue';

export const useUserStore = defineStore('user', () => {
  const accessToken = ref('');
  const refreshToken = ref('');
  const userData = ref(null);

  async function loadUserData() {
    const response_data = await api.get(`me`);
    userData.value = response_data['data'];
  }

  async function logout () {
    accessToken.value = null;
    refreshToken.value = null;
    userData.value = null;
  }

  async function login (username, password) {
    const response = await api.post('accounts/login/', {
      username, password
    });
    const { access, refresh } = response['data'];
    accessToken.value = access;
    refreshToken.value = refresh;
  }
  return {
    accessToken,
    userData,
    login,
    logout,
    loadUserData,
  }
},
{
  persist: true,
});