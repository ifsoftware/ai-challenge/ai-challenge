# Ablauf Kickoff Event

ab 0900 Eintreffen der TN/Sponsoren

915h Start in Aula

## Aula 915h bis 1100h

Vor Aquarium (Gebäude 8, unten) Sponsorenplakate etc?

-> 45 Minuten Blöcke

9:15h

* 5' Begrüssung durch Mitra/Nicola und Vorstellung Tagesablauf
* 20' **Vorstellung und Rede Hauptsponsor**
* 10' Demo
* 10' Vorstellung Challenges (Was müssen die TN tun, was können sie erwarten)

15' Pause und Einschreiben in Team-Listen (evtl. bei Kaffe und Gipfeli?)

10:15h

* **15' Sponsor 2 Vorstellung und Rede**
* 5' Vorstellung und Zuteilung Coaches
* 25' Kurzdurchlauf (Demo) an Beamer von ganzem Ablauf, was Sie machen müssen (inklusive Platformen Vorstellen):
  * Fahren
  * Video Aufnehmen
  * Video Schneiden
  * Lernen
  * Login, JupyterHub, Datenplattform, etc

Pause 15' und Verschiebung nach Bistro

11:15h

* 40' Aufsetzen PCs und Login und Plattformen
* **5' Weitere Sponsoren?**

1200-1300: Mitagessen

13:00h

* 45' Autos übernehmen mit Formular unterschreiben, verbinden mit Hotspot, Zugreifen, Probleme anschauen

14:00h

* 45' Ausfahrt mit Autos, Steckverbdinungen prüfen

Pause 15', eventuell mit Getränken

15:00h

* 45' Abschluss Einführungstag, Plan 8 Wochen Abgabe, Meldeablauf bei Problemen
