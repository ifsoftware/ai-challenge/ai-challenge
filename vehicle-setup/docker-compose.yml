version: "3.7"

services:
  controller: &vehicle
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    user: "root"
    restart: always
    command: ['python', './controller/main.py']
    environment: &env
      LD_LIBRARY_PATH: /opt/vc/lib:$$LD_LIBRARY_PATH
      UPLOADS_BASE_PATH: /data/uploads
      DISPLAY:
      # PYTHONPATH: $$PYTHONPATH:/usr/lib/python3.8/site-packages
      RECORD_EVERY_X_SECONDS: 0.1
      UPDATE_RUNNING_MODE_INFO_X_SECONDS: 0.3
      VIDEO_STREAMING_PORT: 8123
      VIDEO_STREAMING_HOSTNAME: "streaming-server"
      # model to use for self-driving, realtive to the uploads/ folder
      # ie. for uploads/models/my-models.onnx set it to models/my-models.onnx
      SELF_DRIVING_MODEL_PATH: "models/example.onnx"
      # For RCReceiver
      # STEERING_RC_GPIO: 26
      # THROTTLE_RC_GPIO: 19
      # GEAR_RC_GPIO: 13
      # PIGPIO_STEERING_MID: 1500
      # PIGPIO_MAX_FORWARD: 2000
      # PIGPIO_STOPPED_PWM: 1500
      # PIGPIO_MAX_REVERSE: 1000
      # # 0: False, 1: True
      # PIGPIO_INVERT: 0
      # PIGPIO_JITTER: 0.025
      # # for Vehicle
      # STEERING_OFFSET: 10
      # # write out the percentage in decimal. E.g. for a max throttle of 50%, set the value to 0.5.
      # THROTTLE_FACTOR: 1.0
      PI_GPIO_HOST: pigpiod
      PI_GPIO_PORT: 8888
    devices:
    - "/dev/vchiq:/dev/vchiq"
    - "/dev/i2c-0:/dev/i2c-0"
    - "/dev/i2c-1:/dev/i2c-1"
    privileged: true
    volumes:
      - /home/pi/uploads:/data/uploads
      - /home/pi/tmp:/tmp
    logging: &logging
      options:
        max-size: "1m"

  ip-updater:
    restart: always
    user: "root"
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    command: ['python', './vehicle_stats/main.py']
    privileged: true
    network_mode: "host"
    environment:
      <<: *env
    volumes:
      - /home/pi/tmp:/tmp
    logging:
      <<: *logging

  recorder:
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    user: "root"
    restart: always
    command: ['python', './recorder/main.py']
    environment:
      <<: *env
    privileged: true
    volumes:
      - /home/pi/uploads:/data/uploads
      - /home/pi/tmp:/tmp
    logging:
      <<: *logging

  pigpiod:
    # starts pgiopd on port 8888
    image: zinen2/alpine-pigpiod
    user: "root"
    restart: always
    privileged: true
    logging:
      <<: *logging

  receiver:
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    user: "root"
    restart: always
    # entrypoint: ['pigpiod', '-p', '8888']
    command: ['python', './receiver/main.py']
    environment:
      <<: *env
    privileged: true
    devices:
    - "/dev/gpiomem:/dev/gpiomem"
    logging:
      <<: *logging

  access-rights:
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    user: "root"
    command: >-
      bash -c '
      chown -R root:root /data/uploads
      && rm -f /tmp/*.json
      '
    privileged: true
    volumes:
      - /home/pi/uploads:/data/uploads
      - /home/pi/tmp:/tmp
    logging:
      <<: *logging

  redis:
    # stack has timeseries enabled
    # image: redis/redis-stack-server:latest
    image: redis/redis-stack-server@sha256:8b23ad5a68e1a5561bed25feee772ffb3be00f9ae171e65e322e67df587c5886
    # restart after reboot as well
    restart: always
    # disable saving data
    command: >-
      redis-server --protected-mode no --save '' --maxmemory-policy allkeys-lru --appendonly no --maxmemory 250mb --loadmodule /opt/redis-stack/lib/redistimeseries.so RETENTION_POLICY 30
    logging:
      <<: *logging

  streaming-server:
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    user: "root"
    restart: always
    command: ['python', './video_streaming/main.py']
    environment:
      <<: *env
    devices:
      - "/dev/video0:/dev/video0"
      - "/opt/vc:/opt/vc"
    privileged: true
    logging:
      <<: *logging

  vehicle-api:
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle:latest
    user: "root"
    restart: always
    command: ["uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "8381"]
    environment:
      <<: *env
    volumes:
      - /home/pi/uploads:/data/uploads
      - /home/pi/tmp:/tmp
      # needs this to get the log outputs
      - /var/run/docker.sock:/var/run/docker.sock
    logging:
      <<: *logging

  vehicle-frontend:
    # FIXME: runs on port xxxx
    #  FIXME: integrate into proxy service
    restart: always
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/vehicle-frontend:latest
    environment:
      <<: *env
    logging:
      <<: *logging

  proxy:
    image: registry.gitlab.ost.ch:45023/ifs/ai-challenge/image-registry/proxy:latest
    restart: always
    ports:
      - "80:80"
    logging:
      <<: *logging
