#!/bin/bash
set -e
export DEBIAN_FRONTEND=noninteractive

# Update and upgrade
apt-get update -y
apt-get dist-upgrade -y
apt-get install -y python3-setuptools
apt-get autoremove --purge -y

# Activate Camera
wget https://archive.raspberrypi.org/debian/pool/main/r/raspi-config/raspi-config_20220920_all.deb

sudo apt install -y lua5.1 libatopology2 libfftw3-single3 libsamplerate0 alsa-utils ffmpeg
sudo dpkg -i raspi-config_20220920_all.deb
rm -rf raspi-config_20220920_all.deb || true
sudo apt install -y libraspberrypi-bin

sudo mount /dev/mmcblk0p1 /boot || true

if grep -Fxq "start_x=1" /boot/config.txt
then
    echo camera already activated
else
    echo start_x=1 >> /boot/config.txt
fi
raspi-config nonint do_camera 0
raspi-config nonint do_legacy 0

# remove old dependency
sudo killall pigpiod || true
sudo rm -f /usr/local/bin/pigpiod /usr/bin/pigpiod

# Install dependencies

apt-get install -y build-essential i2c-tools avahi-utils joystick git ntp unzip

# Install Docker
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

sudo usermod -aG docker pi

# Get all SSH keys
wget -O - https://gitlab.ost.ch/ifs/infrastructure/server-ssh-keys/-/raw/master/update.sh | bash
mkdir -p /home/pi/.ssh
chown -R pi:pi /home/pi/.ssh
runuser -l pi -c 'wget -O - https://gitlab.ost.ch/ifs/infrastructure/server-ssh-keys/-/raw/master/update.sh | bash'

#Start docker on startup
sudo systemctl enable docker

sudo reboot
