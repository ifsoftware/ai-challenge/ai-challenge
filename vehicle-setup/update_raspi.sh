#!/bin/bash
set -e
export DEBIAN_FRONTEND=noninteractive

sudo apt-get remove -y apache2 || true

sudo chown pi:pi /home/pi/*.sh /home/pi/tmp || true

wget -q --no-check-certificate -O /home/pi/docker-compose.yml https://gitlab.com/ifsoftware/ai-challenge/ai-challenge/-/raw/main/vehicle-setup/docker-compose.yml
wget -q --no-check-certificate -O /home/pi/start_containers.sh https://gitlab.com/ifsoftware/ai-challenge/ai-challenge/-/raw/main/vehicle-setup/start_containers.sh
wget -q --no-check-certificate -O /home/pi/update_containers.sh https://gitlab.com/ifsoftware/ai-challenge/ai-challenge/-/raw/main/vehicle-setup/update_containers.sh
wget -q --no-check-certificate -O /home/pi/setup.sh https://gitlab.com/ifsoftware/ai-challenge/ai-challenge/-/raw/main/vehicle-setup/setup.sh

chmod +x /home/pi/start_containers.sh
chmod +x /home/pi/update_containers.sh
chmod +x /home/pi/setup.sh

rm -f /home/pi/tmp/*.json || true

mkdir -p /home/pi/uploads

/home/pi/update_containers.sh
