# Setting up and building docker images

**NOTICE**: Always build on a Raspberry pi with the correct OS installed (Ubuntu Server 22.04 in 2022),
otherwise some things don't work and we don't know why.

## Automated Setup Raspberry-Pi

After flashing the SD-Card:

1. copy user-data from here to the user-data (Ubuntu 22.04.1 64-bit)
1. change the hostname
1. Insert SD-Card into Raspberry-Pi and let it setup itself.

You should then be able to login with: `pi@<hostname>.local`.

## Building docker image for raspby

Use the `Ubuntu 22.04 Server` Image and flash a new SD-Card
**OR** use an existing already setup car.

1. SSH into the Raspy.
1. Ensure docker is installed: `docker version` (if not, install docker!)
1. ensure you can push your images: `docker login registry.gitlab.ost.ch:45023` (create access token if needed).
1. Clone Repo: `git clone https://gitlab.ost.ch/ifs/ai-challenge/ai-challenge.git`
1. `cd ai-challenge/vehicle-setup`
1. docker compose -f compose.build.yml build --pull
1. docker compose -f compose.build.yml push

```bash
# only needed once
docker login registry.gitlab.ost.ch:45023
git clone https://gitlab.ost.ch/ifs/ai-challenge/ai-challenge.git
cd ai-challenge/vehicle-setup

# repeat
git pull
# don't forget: cd ai-challenge/vehicle-setup ;-)
./build_and_push_images.sh

# to try the setup:
HOSTNAME=$HOSTNAME docker compose up
# or
HOSTNAME=$HOSTNAME docker compose up api
# or
HOSTNAME=$HOSTNAME docker compose up vehicle
```

## This didn't work on my amd64 machine, therefor use the steps above

```bash
# only needed once!
docker run --privileged --rm tonistiigi/binfmt --install all
docker buildx rm builder || true
docker buildx create --name builder --driver docker-container --bootstrap

# list available archs
docker buildx ls

docker buildx use builder
docker buildx build -t  -f Dockerfile --platform linux/arm64,linux/arm --push .
```
