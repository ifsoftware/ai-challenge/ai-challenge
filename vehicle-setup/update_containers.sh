#!/bin/bash
set -e

export HOSTNAME=$HOSTNAME

cd /home/pi

HOSTNAME=$HOSTNAME docker compose down -v
HOSTNAME=$HOSTNAME docker compose pull
HOSTNAME=$HOSTNAME docker compose up -d --remove-orphans
HOSTNAME=$HOSTNAME docker image prune -f
