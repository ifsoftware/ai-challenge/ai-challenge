#!/bin/bash
set -e

export HOSTNAME=$HOSTNAME

cd /home/pi
HOSTNAME=$HOSTNAME docker compose up -d
